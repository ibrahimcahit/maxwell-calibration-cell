#include "DHT.h"
#include "U8glib.h"

#define DHTPIN 16
#define DHTTYPE DHT11

 
#define relayAutopilot 10
#define relayPeltier 9

bool swStat = false;

float h;
float t;
float f;
float hic;
float hif;

signed short minute, second;
char timeline[16];

int state = 0;
int waitTime = 0;
int currTime = 0;
int deltaTime = 0;

DHT dht(DHTPIN, DHTTYPE);
U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_NONE);

void setup() {
  pinMode(relayPeltier, OUTPUT);
  pinMode(relayAutopilot, OUTPUT);
  //Switch off
  digitalWrite(relayPeltier, LOW);
  digitalWrite(relayAutopilot, LOW);
  dht.begin();
}

void loop() {
  h = dht.readHumidity();
  t = dht.readTemperature();
  f = dht.readTemperature(true);

  hic = dht.computeHeatIndex(t, h, false);
  hif = dht.computeHeatIndex(f, h);

  u8g.firstPage();

  switch (state) {
    
    case 0: //Cooling State
    
      digitalWrite(relayPeltier, HIGH);
      digitalWrite(relayAutopilot, LOW);
      u8g.firstPage();
      do {
        coolingState();
      }while (u8g.nextPage() );
      u8g.firstPage();

      if (t <= 10.0){state = 1;}
      else {state = 0;}
      break;

    case 1: //Self Heating State
      digitalWrite(relayPeltier, LOW);
      digitalWrite(relayAutopilot, HIGH);
      u8g.firstPage();
      do {
        selfHeatingState();
      }while (u8g.nextPage() );
      u8g.firstPage();
  
      if (t >= 50.0){
        state = 2;
        waitTime = minute;
        }
      else {state = 1;}
      break;

    case 2: //Waiting State
      digitalWrite(relayPeltier, LOW);
      digitalWrite(relayAutopilot, LOW);
      u8g.firstPage();
      do {
        waitingState();
      }while (u8g.nextPage() );
      u8g.firstPage();
      currTime = minute;
      deltaTime = currTime - waitTime;
      if (deltaTime >= 5){state = 3;}
      else {state = 2;}
      break;

    case 3:
      digitalWrite(relayPeltier, LOW);
      digitalWrite(relayAutopilot, LOW);
      u8g.firstPage();
      do {
        done();
      }while (u8g.nextPage() );
      u8g.firstPage();

      state = 3;
      break;
    
  }

  delay(999);
  
  u8g.setColorIndex(0);
  u8g.drawBox(1, 1, 64, 128);
  u8g.setColorIndex(1);
  
  second++;
  if (second == 60)
  {
    second = 0;
    minute ++;
  }
  sprintf(timeline,"%0.2d:%0.2d", minute, second);
}

//******************
// STATE FUNCTIONS
//******************

void coolingState() {
  u8g.setFont(u8g_font_profont12);
  u8g.setPrintPos(0, 10);
  u8g.print("State: Cooling");
  
  u8g.setPrintPos(0, 25);
  u8g.print("Temperature: ");
  u8g.setPrintPos(75, 25);
  u8g.print(t);
  
  u8g.setPrintPos(0, 40);
  u8g.print("Humidity: ");
  u8g.setPrintPos(55, 40);
  u8g.print(h);  

  u8g.setPrintPos(0, 55);
  u8g.print("Time: ");  
  u8g.setPrintPos(33, 55);
  u8g.print(timeline); 
}

void waitingState() {
  u8g.setFont(u8g_font_profont12);
  u8g.setPrintPos(0, 10);
  u8g.print("State: Waiting");
  
  u8g.setPrintPos(0, 25);
  u8g.print("Temperature: ");
  u8g.setPrintPos(75, 25);
  u8g.print(t);
  
  u8g.setPrintPos(0, 40);
  u8g.print("Humidity: ");
  u8g.setPrintPos(55, 40);
  u8g.print(h);  

  u8g.setPrintPos(0, 55);
  u8g.print("Time: ");  
  u8g.setPrintPos(33, 55);
  u8g.print(timeline); 
}

void selfHeatingState() {
  u8g.setFont(u8g_font_profont12);
  u8g.setPrintPos(0, 10);
  u8g.print("State: Self Heating");
  
  u8g.setPrintPos(0, 25);
  u8g.print("Temperature: ");
  u8g.setPrintPos(75, 25);
  u8g.print(t);
  
  u8g.setPrintPos(0, 40);
  u8g.print("Humidity: ");
  u8g.setPrintPos(55, 40);
  u8g.print(h);  

  u8g.setPrintPos(0, 55);
  u8g.print("Time: ");  
  u8g.setPrintPos(33, 55);
  u8g.print(timeline); 
}

void done() {
  u8g.setFont(u8g_font_profont12);
  u8g.setPrintPos(0, 10);
  u8g.print("State: Done!");
  
  u8g.setPrintPos(0, 25);
  u8g.print("Temperature: ");
  u8g.setPrintPos(75, 25);
  u8g.print(t);
  
  u8g.setPrintPos(0, 40);
  u8g.print("Humidity: ");
  u8g.setPrintPos(55, 40);
  u8g.print(h);  

  u8g.setPrintPos(0, 55);
  u8g.print("Time: ");  
  u8g.setPrintPos(33, 55);
  u8g.print(timeline); 
}
